$(function(){
    /* 控制侧边栏的一级菜单 */
    (function firstMenu(){
        let laststr;
        $(".menu-first").on("click",
            function(e){
                $("#data-analysis-box span").hide();
                let str = "#"+e.delegateTarget.id+"-box"+" "+"span";
                if(laststr) $(laststr).hide();         
                laststr = str;
                $(str).show(500);
            }
        );
    })();
    /* 控制侧边栏的二级菜单 */
    (function secondMenu(){
        let laststr;
        $(".menu-second span").on("click",
        function(e){
            let str = "#show-"+e.delegateTarget.id;
            if(laststr) $(laststr).hide();  
            laststr = str;
            console.log(str)
            $(str).show(500);
        }
    );
    })();
    /* 控制模型训练页的页面选择 */
    (function choosePage(){
        let laststr;
        $(".model-training-menu span").on("click",
        function(e){
            $("#show-content-part-3-1").hide(200);
            let str = "#"+e.delegateTarget.className.slice(0,-4);
            if(laststr) $(laststr).hide();  
            laststr = str;
            console.log(str)
            $(str).show(500);
        }
    );
})();
}
)