//     var myChartlt = echarts.init(document.getElementById("crc-lt-chart"));
//     var myChartlb = echarts.init(document.getElementById("crc-lb-chart"));
//     var myChartrt = echarts.init(document.getElementById("crc-rt-chart"));
//     var myChartrb = echarts.init(document.getElementById("crc-rb-chart"));
    
//  option5 = {
//  grid:{
//                     x:'10%',
//                     y:'20%',
//                     x2:'10%',
//                     y2:'10%',
//                     borderWidth:1
//                 },
//     tooltip : {
//         trigger: 'axis',
//         axisPointer: {
//             type: 'cross',
//             label: {
//                 backgroundColor: '#6a7985'
//             }
//         }
//     },
//   // legend: {
//   //       data:['1','2','3']
//   //   },
//     xAxis: {
//         type: 'category',
//         data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
//          axisLabel : {
//                 show:false,
//                 formatter: '{value}'
//             }
//     },
//     yAxis: {
//         type: 'value',
//          axisLabel : {
//                 show:false,
//                 formatter: '{value}'
//             }

//     },
//     series: [{
//         name:'1',
//         data: [820, 932, 801, 634, 1290, 1330, 1320],
//         type: 'line',
//         smooth: true
//     },{
//         name:'2',
//         data: [720, 832, 701, 1034, 1490, 1130, 1520],
//         type: 'line',
//         smooth: true
//     },{
//         name:'3',
//         data: [520, 632, 501, 1134, 1190, 1530, 1720],
//         type: 'line',
//         smooth: true
//     }
//     ]
// };
// option6 = {
//   tooltip:{
//   trigger: 'axis',
//         axisPointer: {
//             type: 'cross',
//             label: {
//                 backgroundColor: '#6a7985'
//             }
//         }
//   },
//     grid:{
//         x:'10%',
//         y:'20%',
//         x2:'10%',
//         y2:'10%',
//     },
//     xAxis: {
//         type: 'category',
//         data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
//         axisLabel:{
//             show:false,
//         }
//     },
//     yAxis: {
//         type: 'value',
//         axisLabel:{
//             show:false,
//         }
//     },
//     series: [{
//         data: [7, 9, 8, 1, 6, 8, 4],
//         type: 'line',
//         smooth: true
//     }]
// };
// option7 = {
//     tooltip : {
//         trigger: 'axis',
//         axisPointer: {
//             type: 'cross',
//             label: {
//                 backgroundColor: '#6a7985'
//             }
//         }
//     },
//     grid: {
//         x:'10%',
//         y:'20%',
//         x2:'10%',
//         y2:'10%',      
//     },
//     xAxis : [
//         {
//             type : 'category',
//             boundaryGap : false,
//             data : ['周一','周二','周三','周四','周五','周六','周日'],
//             axisLabel:{
//               show:false,
//             }
            
//         }
//     ],
//     yAxis : [
//         {
//             type : 'value',
//             axisLabel:{
//                 show:false,
//             }
//         }
//     ],
//     series : [
//         {
//             name:'邮件营销',
//             type:'line',
//             stack: '总量',
//             areaStyle: {},
//             data:[12, 132, 101, 134, 90, 230, 0],
//             smooth:true
//         },
//         {
//             name:'联盟广告',
//             type:'line',
//             stack: '总量',
//             areaStyle: {},
//             data:[20, 182, 191, 234, 290, 230, 0],
//             smooth:true
            
//         },
//         {
//             name:'视频广告',
//             type:'line',
//             stack: '总量',
//             areaStyle: {},
//             data:[10, 232, 201, 154, 390, 30, 10],
//             smooth:true
            
//         },
//         {
//             name:'直接访问',
//             type:'line',
//             stack: '总量',
//             areaStyle: {normal: {}},
//             data:[0, 332, 301, 334, 390, 230, 120],
//             smooth:true
            
//         },
//         {
//             name:'搜索引擎',
//             type:'line',
//             stack: '总量',
//             label: {
//                 normal: {
//                     show: false,
//                     position: 'top'
//                 }
//             },
//             areaStyle: {normal: {}},
//             data:[20, 432, 901, 934, 390, 130, 20],
//             smooth:true
            
//         }
//     ]
// };

//     myChartlb.setOption(option5);
//     myChartlt.setOption(option6);
//     myChartrb.setOption(option7);
//   window.onresize = function(){
//     myChartlb.resize();
//     myChartlt.resize();
//     myChartrb.resize();
//     // 
//     myCharts1.resize();
//     myCharts2.resize();
//    // 初始界面的图
//    myChart1.resize();

//     myChart2.resize();

//     myChart3.resize();

//     myChart4.resize();

//   }
