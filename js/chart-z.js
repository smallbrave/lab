   var myChart1 = echarts.init(document.getElementById('chart-1'));
   var myChart2 = echarts.init(document.getElementById('chart-2'));
   var myChart3 = echarts.init(document.getElementById('chart-3'));
   var myChart4 = echarts.init(document.getElementById('chart-4'));
       var myChartlt = echarts.init(document.getElementById("crc-lt-chart"));
    var myChartlb = echarts.init(document.getElementById("crc-lb-chart"));
    var myChartrt = echarts.init(document.getElementById("crc-rt-chart"));
    var myChartrb = echarts.init(document.getElementById("crc-rb-chart"));
      var myCharts1 = echarts.init(document.getElementById("left"));
  var myCharts2 = echarts.init(document.getElementById("right"));
option1 = {
  tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    grid:{
                    x:10,
                    y:0,
                    x2:10,
                    y2:0,
                    borderWidth:1
                },
    xAxis: {
        type: 'category',
        // data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
       axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },
    },
    yAxis: {
        type: 'value',
          axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },axisLabel: {
                show:false,
                   textStyle: {
                       color: '#ccc',
                       fontSize:8,
                   }
               }
    },

    series: [{
        data: [1, 9, 1, 3, 7, 1, 2],
        type: 'line',
        color:'#f98377',
    },{
        data: [1, 3, 1, 9, 1, 6, 9],
        type: 'line',
        color:'#557773',
    }]
};
    option2 = {
      tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
        grid:{
                    x:10,
                    y:0,
                    x2:10,
                    y2:0,
                    borderWidth:1
                },
    xAxis: {
        type: 'category',
        // data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
       axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },
    },
    yAxis: {
        type: 'value',
          axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },axisLabel: {
                show:false,
                   textStyle: {
                       color: '#ccc',
                       fontSize:8,
                   }
               }
    },

    series: [{
        data: [1, 9, 1, 3, 7, 1, 2],
        type: 'bar',
        color:'#f98377',
    },{
        data: [1, 3, 1, 9, 1, 6, 9],
        type: 'bar',
        color:'#557773',
    }]
};
option3 = {
  tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
         grid:{
                    x:20,
                    y:0,
                    x2:20,
                    y2:0,
                    borderWidth:1
                },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            show:false,
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                show:false,
                   textStyle: {
                       color: '#ccc',
                       fontSize:8,
                   }
               }
        },
        {
          type:'value'
        }
      
    ],
    series : [
        {
            name:'成交',
            type:'line',
            smooth:true,
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:[10, 12, 21, 54, 260, 830, 710]
        },
        {
            name:'预购',
            type:'line',
            smooth:true,
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:[30, 182, 434, 791, 390, 30, 10]
        },
        {
            name:'意向',
            type:'line',
            smooth:true,
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:[1320, 1132, 601, 234, 120, 90, 20]
        }
    ]
};
option4 = {
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
     grid:{
                    x:20,
                    y:4,
                    x2:20,
                    y2:2,
                    borderWidth:1
                },
    toolbox: {
        show : false,
        feature : {
            mark : {show: false},
            dataView : {show: true, readOnly: false},
            magicType: {show: true, type:'line'},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
   
    xAxis : [
        {
            type : 'category',
            data : ['1月','2月','3月','4月','5月','6月','7月'],
         axisLine:{
                 lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2',
                       height:'100%',
                   }
        }
    }
    ],
    yAxis : [
        {
            type : 'value',
            axisLine:{
                 lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2',
                       height:'100%',
                   }
              
            },
            axisLabel : {
                show:false,
                formatter: '{value}'
            }
        },
        {
            type : 'value',
            axisLabel : {
                show:false,
                formatter: 'value'
            }, axisLine:{
                 lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2',
                       height:'100%',
                   }
               }
        }
    ],
    series : [

        {
            name:'蒸发量',
            type:'line',
            data:[5.0, 4.9, 7.0, 23.2, 25.6,  162.2, 32.6, ]
        },
        {
            name:'降水量',
           type:'line',
            data:[2.6, 5.9, 9.0, 70.7, 175.6, 48.7, 18.8, 6.0, 2.3]
        },       
      {
            name:'降雨量',
           type:'line',
            data:[3.6, 6.9, 19.0, 26.4, 30.7, 60.7, 155.6, 162.2, 48.7, 18.8, 6.0, 2.3]
        },
        {
            name:'平均温度',
            type:'line',
            yAxisIndex: 1,

            data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
        }
    ]
};
                    
                    
                                    
    myChart1.setOption(option1);
    myChart2.setOption(option2);
    myChart3.setOption(option3);
    myChart4.setOption(option4);
    
 option5 = {
 grid:{
                    x:'10%',
                    y:'20%',
                    x2:'10%',
                    y2:'10%',
                    borderWidth:1
                },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
  // legend: {
  //       data:['1','2','3']
  //   },
    xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
         axisLabel : {
                show:false,
                formatter: '{value}'
            }
    },
    yAxis: {
        type: 'value',
         axisLabel : {
                show:false,
                formatter: '{value}'
            }

    },
    series: [{
        name:'1',
        data: [820, 932, 801, 634, 1290, 1330, 1320],
        type: 'line',
        smooth: true
    },{
        name:'2',
        data: [720, 832, 701, 1034, 1490, 1130, 1520],
        type: 'line',
        smooth: true
    },{
        name:'3',
        data: [520, 632, 501, 1134, 1190, 1530, 1720],
        type: 'line',
        smooth: true
    }
    ]
};
option6 = {
  tooltip:{
  trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
  },
    grid:{
        x:'10%',
        y:'20%',
        x2:'10%',
        y2:'10%',
    },
    xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        axisLabel:{
            show:false,
        }
    },
    yAxis: {
        type: 'value',
        axisLabel:{
            show:false,
        }
    },
    series: [{
        data: [7, 9, 8, 1, 6, 8, 4],
        type: 'line',
        smooth: true
    }]
};
option7 = {
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    grid: {
        x:'10%',
        y:'20%',
        x2:'10%',
        y2:'10%',      
    },
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            axisLabel:{
              show:false,
            }
            
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel:{
                show:false,
            }
        }
    ],
    series : [
        {
            name:'邮件营销',
            type:'line',
            stack: '总量',
            areaStyle: {},
            data:[12, 132, 101, 134, 90, 230, 0],
            smooth:true
        },
        {
            name:'联盟广告',
            type:'line',
            stack: '总量',
            areaStyle: {},
            data:[20, 182, 191, 234, 290, 230, 0],
            smooth:true
            
        },
        {
            name:'视频广告',
            type:'line',
            stack: '总量',
            areaStyle: {},
            data:[10, 232, 201, 154, 390, 30, 10],
            smooth:true
            
        },
        {
            name:'直接访问',
            type:'line',
            stack: '总量',
            areaStyle: {normal: {}},
            data:[0, 332, 301, 334, 390, 230, 120],
            smooth:true
            
        },
        {
            name:'搜索引擎',
            type:'line',
            stack: '总量',
            label: {
                normal: {
                    show: false,
                    position: 'top'
                }
            },
            areaStyle: {normal: {}},
            data:[20, 432, 901, 934, 390, 130, 20],
            smooth:true
            
        }
    ]
};

    myChartlb.setOption(option5);
    myChartlt.setOption(option6);
    myChartrb.setOption(option7);
      option8 = {
      grid:{
                    x:'10%',
                    y:'10%',
                    x2:'10%',
                    y2:'10%',
                    borderWidth:1
                },
    xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Sat', 'Sun'],
       axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },
    },
   calculable : true,
    yAxis: {
        type: 'value',
          axisLine: {
                   lineStyle: {
                       type: 'solid',
                       color:'#5c6475',
                       width:'2'
                   }
               },axisLabel: {
                show:true,
                   textStyle: {
                       color: '#ccc',
                       fontSize:8,
                   }
               }
    },

    series: [{
        data: [1, 2, 3, 4, 5],
        type: 'bar',
        color:'#f98377',
    }]
};
  option9 = {
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    calculable : true,
    series : [
        {
            name:'访问来源',
            type:'pie',
            radius : ['50%', '70%'],
            itemStyle : {
                normal : {
                    label : {
                        show : false
                    },
                    labelLine : {
                        show : false
                    }
                },
                emphasis : {
                    label : {
                        show : true,
                        position : 'center',
                        textStyle : {
                            fontSize : '30',
                            fontWeight : 'bold'
                        }
                    }
                }
            },
            data:[
                {value:335, name:'直接访问'},
                {value:310, name:'邮件营销'},
                {value:234, name:'联盟广告'},
                {value:135, name:'视频广告'},
                {value:1548, name:'搜索引擎'}
            ]
        }
    ]
};
                                      
  myCharts1.setOption(option8);
  myCharts2.setOption(option9);

  window.onresize = function(){
    myChartlb.resize();
    myChartlt.resize();
    myChartrb.resize();
    // 
    myCharts1.resize();
    myCharts2.resize();
   // 初始界面的图
   myChart1.resize();

    myChart2.resize();

    myChart3.resize();

    myChart4.resize();

  }
